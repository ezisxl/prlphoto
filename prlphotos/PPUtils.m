//
//  PPUtils.m
//  prlphotos
//
//  Created by Ansis on 22/04/14.
//  Copyright (c) 2014 Applabs. All rights reserved.
//

#import "PPUtils.h"

@implementation PPUtils

+ (UIColor*)darkColor
{
    return [UIColor colorWithWhite:0.110 alpha:1.000];
}

@end
