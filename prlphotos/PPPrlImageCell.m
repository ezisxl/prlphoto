//
//  PPPrlImageCell.m
//  prlphotos
//
//  Created by Ansis on 22/04/14.
//  Copyright (c) 2014 Applabs. All rights reserved.
//

#import "PPPrlImageCell.h"

#define offset 70

@interface PPPrlImageCell ()


@property (nonatomic, strong) UIImageView *parallaxImage;

@end

@implementation PPPrlImageCell
@synthesize parallaxImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor: [PPUtils darkColor]];

        UIView *masterView = [[UIView alloc] initWithFrame: CGRectMake(0, 2, self.contentView.frame.size.width, imageCellHeight-4)];
        [masterView setBackgroundColor: [UIColor lightGrayColor]];
        [masterView setUserInteractionEnabled: YES];
        [self.contentView addSubview: masterView];
        
        parallaxImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, -offset, self.contentView.frame.size.width, imageCellHeight+offset*2)];
        [parallaxImage setOpaque: YES];
        [parallaxImage setBackgroundColor: [UIColor lightGrayColor]];
        
        
        [masterView.layer setMasksToBounds: YES];
        [masterView addSubview: parallaxImage];
        
        [self.contentView setAutoresizesSubviews: YES];
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view
{
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    
    float distanceFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.parallaxImage.frame) - CGRectGetHeight(self.frame);
    float move = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference;

    CGRect imageRect = self.parallaxImage.frame;
    imageRect.origin.y = -(difference/2)+move;
    parallaxImage.frame = imageRect;
}

- (void)updateWithImage:(UIImage*)i
{
    [parallaxImage setImage: i];
}

@end
