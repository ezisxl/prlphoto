//
//  PPPrlImageCell.h
//  prlphotos
//
//  Created by Ansis on 22/04/14.
//  Copyright (c) 2014 Applabs. All rights reserved.
//

#import <UIKit/UIKit.h>

#define imageCellHeight 200

@interface PPPrlImageCell : UITableViewCell


- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;
- (void)updateWithImage:(UIImage*)i;

@end
