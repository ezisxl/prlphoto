//
//  PPUtils.h
//  prlphotos
//
//  Created by Ansis on 22/04/14.
//  Copyright (c) 2014 Applabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPUtils : NSObject

+ (UIColor*)darkColor;

@end
