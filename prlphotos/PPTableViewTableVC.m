//
//  PPTableViewTableVC.m
//  prlphotos
//
//  Created by Ansis on 22/04/14.
//  Copyright (c) 2014 Applabs. All rights reserved.
//

#import "PPTableViewTableVC.h"

#import "PPAppDelegate.h"
#include <AssetsLibrary/AssetsLibrary.h>


//helpers
#import "ClusterPrePermissions.h"

//views
#import "MRProgressOverlayView.h"
#import "PPPrlImageCell.h"


@interface PPTableViewTableVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) NSArray *pPhotoArray;
@property (nonatomic, strong) UITableView *pTableview;
@property (nonatomic, strong) ALAssetsLibrary *library;
@property (nonatomic, strong) MRProgressOverlayView *pProgressView;

@end

@implementation PPTableViewTableVC
@synthesize pTableview;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {


    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor: [PPUtils darkColor]];
    
    pTableview = [[UITableView alloc]initWithFrame:self.view.frame style: UITableViewStylePlain];
    [pTableview setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview: pTableview];
    
    [pTableview setSeparatorStyle: UITableViewCellSeparatorStyleNone];
    [pTableview setDelegate: self];
    [pTableview setDataSource: self];
    

    ClusterPrePermissions *permissions = [ClusterPrePermissions sharedPermissions];
    [permissions showPhotoPermissionsWithTitle:@"Piekļūt bildēm?"
                                       message:@"Drīkst?"
                               denyButtonTitle:@"Nē, vēlāk"
                              grantButtonTitle:@"Var"
                             completionHandler:^(BOOL hasPermission,
                                                 ClusterDialogResult userDialogResult,
                                                 ClusterDialogResult systemDialogResult) {
                                 if (hasPermission) {

                                     _pProgressView = [MRProgressOverlayView new];
                                     _pProgressView.mode = MRProgressOverlayViewModeIndeterminate;
                                     [[[PPAppDelegate sharedInstance] window] addSubview: _pProgressView];
                                     [_pProgressView setTitleLabelText: @"Lādē bildes…"];
                                     [_pProgressView show:YES];
                                     

                                     
                                     [self p_allLoaded];
                                     

                                 } else {

                                 }
                             }];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self scrollViewDidScroll:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return imageCellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_pPhotoArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cid = @"cell";
    PPPrlImageCell *cell = [tableView dequeueReusableCellWithIdentifier: cid];
    if (!cell){
        cell = [[PPPrlImageCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: cid];
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self p_loadImage: indexPath url: _pPhotoArray[indexPath.row] ];
    });
    
    return cell;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSArray *visibleCells = [pTableview visibleCells];
    
    for (PPPrlImageCell *cell in visibleCells) {
        [cell cellOnTableView:pTableview didScrollOnView:self.view];
    }
}




#pragma mark Private meths

- (void)p_loadImage:(NSIndexPath *)indexPath url:(NSURL*)url
{
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        CGImageRef iref = [[myasset defaultRepresentation] fullScreenImage];
        if (iref) {
            UIImage *i = [[UIImage alloc] initWithCGImage: iref];
            NSDictionary *result = @{@"image" : i, @"index" : indexPath};
            [self performSelectorOnMainThread:@selector(p_imageReady:) withObject:result waitUntilDone:NO];
        }
    };

    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        
    };
    
    [[self p_defaultAssetsLibrary] assetForURL:url
                                 resultBlock:resultblock
                                failureBlock:failureblock];
}

- (void)p_imageReady:(NSDictionary *) result
{
    UIImage *i = result[@"image"];
    NSIndexPath *ip = result[@"index"];
    PPPrlImageCell *cell = (PPPrlImageCell*)[pTableview cellForRowAtIndexPath: ip];
    [cell updateWithImage: i];
}



- (void)p_allLoaded
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *urls = [NSMutableArray new];
        ALAssetsLibrary *assetLibrary = [self p_defaultAssetsLibrary];
//float onePercent = (float)(100.f / reccords);        
        [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                                    usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                        if (group) {
                                            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                                            [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
                                                
                                                if (asset) {
                                                    ALAssetRepresentation *defaultRepresentation = [asset defaultRepresentation];
                                                    NSString *uti = [defaultRepresentation UTI];
                                                    NSURL  *videoURL = [[asset valueForProperty:ALAssetPropertyURLs] valueForKey:uti];
                                                    [urls addObject: videoURL];
                                                    
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                                        float done = (onePercent * count+1) / 100;
//                                                        [_pProgressView setProgress: done > 1 ? 1 : done];// _pProgressView.progress + onePercent];
//                                                    });
                                                    
                                                }
                                                
                                            }];
                                            
                                            
                                        } else {
                                            [self performSelectorOnMainThread: @selector(p_moveOn:) withObject: urls waitUntilDone: NO];
                                            
                                        }
                                        
                                    } failureBlock:^(NSError *error) {
                                        
                                    }
         ];
    });
    
}

- (void)p_moveOn:(NSMutableArray*)a{

    _pProgressView.mode = MRProgressOverlayViewModeCheckmark;
    _pProgressView.titleLabelText = @"Gatavs!";
    [self p_performBlock:^{
        [_pProgressView dismiss:YES];
    } afterDelay:0.7];
    
    _pPhotoArray = [NSArray arrayWithArray: a];
    [pTableview reloadData];
}

- (ALAssetsLibrary *)p_defaultAssetsLibrary {
    if (_library == nil) {
        _library = [[ALAssetsLibrary alloc] init];
    }
    return _library;
}

- (void)p_performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

@end
